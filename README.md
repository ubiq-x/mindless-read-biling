# Eye Movements of Bilinguals during Mindful and Mindless Reading
An experiment for collecting eye movements indigenous to normal (i.e., mindful) and mindless reading of English and Spanish text corpora to be used with SR Research [Experiment Builder](https://www.sr-research.com/experiment-builder).


## Abstract
_Mind-wandering_ is a phenomenon of having thoughts unrelated to the immediate task context.  High incidence of mind-wandering has been found to correlate with low performance on many task (Smallwood, Fishman, & Schooler, 2007).  One reason why mind-wandering impacts performance so negatively is that its occurrences may go totally unnoticed by the person who lapses into it.  Mind-wandering has been documented in a wide range of activities (Killingsworth & Gilbert, 2010).  One such activity is reading.  When mind-wandering occurs during reading it is often referred to as _mindless reading_ because while the reader's eyes continue moving across printed text in a reading-like manner, little or nothing is actually understood.  Indeed, frequent lapses into mindless reading are not conducive to good text comprehension (Schooler, Reichle, & Halpern, 2004).

The theories of _Global Workspace_ (Baars, 1988) and _Global Neuronal Workspace_ (Dehaene & Naccache, 2001) suggest that activities which leave enough "free" working memory may make people more prone to mind-wandering because those unused mental resources can be engaged through, among others, spontaneous thoughts that can quickly dominate the content of the consciousness.  Those theories therefore predict that individuals more taxed by a task should find themselves mind-wandering less.  Bilinguals exhibit higher levels of cognitive engagement when reading compared to monolinguals.  For example, when processing language, bilinguals experience automatic co-activation of both languages (e.g., Marian & Spivey, 2003; Thierry & Wu, 2007).  Therefore, one hypothesis consistent with the global workspace would be that bilinguals should be more resistant to lapsing into mindless reading.  The experiment I publish here has been designed to investigate this hypothesis.


## Contents
- [Features](#features)
- [Details](#details)
  * [Mindless Reading](#mindless-reading)
  * [Sampling Mindless Reading](#sampling-mindless-reading)
  * [Trials](#trials)
  * [Subject Types](#subject-types)
  * [Instructions and Stimuli](#instructions-and-stimuli)
  * [Stimuli Presentation Schedules](#stimuli-presentation-schedules)
  * [Experiment Builder Variables](#experiment-builder-variables)
  * [Results](#results)
  * [Medusa Integration](#medusa-integration)
- [Data Analysis](#data-analysis)
  * [Data Preparation](#data-preparation)
  * [Graphical Output](#graphical-output)
  * [Textual Output](#textual-output)
- [Dependencies](#dependencies)
- [Setup](#setup)
- [Acknowledgments](#acknowledgments)
- [References](#references)
- [Citing](#citing)
- [License](#license)


# Features
- Collect eye movements occurring during normal (i.e., mindful) and mindless reading of English and Spanish text corpora (with easy extension for other languages)
- Utilizes two kinds of mindless reading sampling methods: (1) Localized questions and (2) thought sampling combined with self-reports
- Requires SR Research [Experiment Builder](https://www.sr-research.com/experiment-builder)


## Details
### Mindless Reading
Mindless reading is a phenomenon that occurs when a reader's eyes continue moving across printed text in a reading-like manner but with little or nothing being understood.  Its incidence is higher than introspection might reveal.  For example, when readers are asked about being on- or off-task at unpredictable random intervals, they can be found to be reading mindlessly even a quarter of the time (Reichle, Reineberg, & Schooler, 2010; Loboda, 2014).  Certain factors make mindless reading more or less likely.  For example, being engaged with an interesting topic makes it less likely (Grodsky & Giambra, 1989) while craving cigarettes makes it harder to stay focused on what is being read (Sayette, Schooler, & Reichle, 2010).

### Sampling Mindless Reading
#### Localized questions
This experiment employs two mindless reading sampling procedures.  The first one does not have precedence in the literature (to my knowledge) and comes in the form of localized questions.  These questions are carefully crafted to ensure that the answer to any one of them is contained in a small passage of text (being from several words to several sentences long).  Moreover, the answer to any of these questions cannot be known from the text that precedes the passage containing the answer.  This design allows to attribute inability to provide the correct answer to an instance of mindless reading experienced while reading of the associated passage.  Unfortunately, this design also confounds mindless reading with the memory (or lack thereof) of the associated passage (i.e., the subject can simply forget what they read).

When presenting a localized question to the subject, the experiment will permit them to choose one of the answers using a keyboard.  However, it will also permit one of the following responses:

- A) I don't know, and I don't even remember reading about that
- B) I don't know, but I do remember reading about that
- C) I know that from previous chapters

These three choices are there to discourage the subjects from guessing because a correct but guessed answer would be treated as an instance of mindful reading and thereby contaminate the results.  Choice A is akin to admitting to have lapsed into mindless reading.  Choice B is an attempt to directly address the issue of confounding memory of the answer with mindless reading while reading the answer.  Choice C is a safety valve for cases of questions accidentally asking about information that has been revealed earlier in text (i.e., incorrectly formulated localized questions).

I was responsible for creating localized questions for the original of Jane Austen's _Sense and Sensibility_ and Beatriz Barragan was kind enough to take on that task for the Spanish translation of the novel.  All localized questions were created using the [Medusa](https://gitlab.com/ubiq-x/medusa) software.

#### Probes and Self-Reports
The second procedure, _thought sampling_ (Smallwood & Schooler, 2006), has been used multiple times.  In thought sampling, the subject is periodically probed at unpredictable times and the goal of the probes is to discover if the subject's attention is on-task (i.e., if they are reading mindfully) or off-task (i.e., if they are reading mindlessly).  As is typically done, this design is further supplemented with the instruction to self-report mindless to allow for sampling mindless reading that would otherwise be missed by probes alone.  This combination of probes and self-reports allows for the discovery of the so called _probe-caught_ mindless reading and _self-caught_ mindless reading.

### Trials
The two mindless reading sampling methods described above are deployed in two consecutive trials that comprise the experiment.  They are called _phases_ in the Experiment Builder project itself.  Localized questions are used in the first trial (i.e., Phase 1); probes and self-reports are used in the second trial (i.e., Phase 2).  The experiment makes an automatic transition from the first to the second trial, i.e., no experimenter action is required for that to happen.  This is just one way in which potential human errors are eliminated.  In the Experiment Builder project, all items related to the tho trials (or phases) are prefixed or suffixed with `p1` or `p2`.

The second trial begins after a defined time has passed.  That time is stored in the `CONST_EXP_P1_DUR` [variable](#experiment-builder-variables) in the Experiment Builder project and is given in milliseconds.  The transition is not abrupt, however, in that it only occurs after the current chapter has ended.  That ensures that the reader will finish every chapter in the trial they begun it in.  Because the second trial requires new instructions, this implementation is methodologically sound in that it does not interrupt the reader mid-chapter.  Moreover, it also simplifies subsequent statistical analysis of the eye-movement data, because no mid-chapter data slicing is required.

### Subject Types
Before the task commences, the experimenter selects the type of the subject being run:

- Monolingual English
- Monolingual Spanish
- Bilingual

This selection is significant because monolinguals only go through the task corresponding to their language while bilinguals go through both English and Spanish versions of the text.

### Instructions and Stimuli
Subject instructions and all stimuli (i.e., pages of text) are saved as sets of images in the [`eb/library/images`](eb/library/images) directory.  Relying on images instead of text is space inefficient but allows to sidestep all problems related to improper rendition of text of different computers which is especially important when diacritics are involved as well.  For space efficiency, only one sets of images is included in this repository; that set corresponds to the `1680x1050` screen resolutions.

If different resolution is required, new images need to be generated (e.g., using the [Medusa](https://gitlab.com/ubiq-x/medusa) software) or the existing ones need to be rescaled; the new images need to be named properly because the experiment chooses them based on their name.  Moreover, the screen resolution defined in the Experiment Builder project properties needs to be changed accordingly as well.

<kbd>![Sense and Sensibility: Chapter 1, Page 1](eb/library/images/1680x1050-en-c01p01.png)</kbd>
**Figure 1.** The first page of _Sense and Sensibility_.<br /><br />

<kbd>![Sense and Sensibility: Localized question](eb/library/images/1680x1050-en-c01p01q03.png)</kbd>
**Figure 2.** Localized question example.<br /><br />

For demonstration purposes, only the first chapter of _Sense and Sensibility_ is included, both in English and Spanish.  All localized questions for those two chapters are included as well.  Moreover, both chapters include four end-of-chapter comprehension questions which are used to establish if subject exert effort to understand the text, a practice important in any study of reading for comprehension.  Figure 1 shows the first page of the book and Figure 2 shows one of localized questions.

Finally, the mindless reading probe is included as well, both in [English](eb/library/images/1680x1050-en-zo-probe.png) and [Spanish](eb/library/images/1680x1050-sp-zo-probe.png).

### Stimuli Presentation Schedules
The present experiment allows the presentation of large corpora of text.  Moreover, the experiment allows the use of image stimuli with different resolutions.  Changing the screen resolution typically results in change in text pagination, i.e., the text is layed out different across pages for different resolutions.  Of course, the font size, number of characters per line, and number of lines per page influence pagination as well.  To make the Experiment Builder project independent of changes in resolution and pagination, the project itself does not need to be changed.  Instead, two tab-delimited text files control the stimuli presentation schedule.  That is not ordinarily possible in Experiment Builder and has been achieved through the use of a Python custom class [`eb/library/customClass/LoadLists.py`](eb/library/customClass/LoadLists.py) that loads the text files upon the experiment launch.  Editing these files in a simple text editor (e.g., changing the order of rows) is all that is necessary to edit the stimuli presentation schedule in either of the two phases.  The two files are described below.

#### Localized Questions Phase
The file [`eb/myfiles/1680x1050-en-eb-tbl-q1-random.txt`](eb/myfiles/1680x1050-en-eb-tbl-q1-random.txt) contains the schedule for the first trial (i.e., Phase 1) of the experiment.  This is how the first chapter of _Sense and Sensibility_ is encoded for the `1680x1050` screen resolution (tabs replaced with spaces for clarity):
```
name        type  ch_num  pg_num  q_num_ch  q_num_pg  q_id  a_cnt  a_ord  a_corr_num  q_word_a  q_word_b
title       t          0       0         0         0     0      0      .           0         0         0
c01         t          1       0         0         0     0      0      .           0         0         0
c01p01      t          1       1         0         0     0      0      .           0         0         0
c01p01q01   q          1       1         1         1   214      4   3214           3         8        10
c01p01q02   q          1       1         2         2   215      4   4321           4        86        94
c01p01q03   q          1       1         3         3   216      4   2314           3       117       128
c01p01q04   q          1       1         4         4   217      4   4213           3       163       185
c01p01q05   q          1       1         5         5   218      3    321           3       210       216
c01p01q06   q          1       1         6         6   219      4   2134           2       218       225
c01p02      t          1       2         0         0     0      0      .           0         0         0
c01p03      t          1       3         0         0     0      0      .           0         0         0
c01p04      t          1       4         0         0     0      0      .           0         0         0
c01p05      t          1       5         0         0     0      0      .           0         0         0
c01p02q01   q          1       2         7         1   220      4   1234           1       353       368
c01p03q01   q          1       3         8         1   221      4   3142           2       790       809
c01p03q02   q          1       3         9         2   222      4   3421           4       901       910
c01p04q01   q          1       4        10         1   223      4   2314           3      1255      1257
c01p05q01   q          1       5        11         1   224      4   2134           2      1508      1511
c01q01b001  qe         1       0         1         0     0      4      .           3         0         0
c01q02b002  qe         1       0         2         0     0      4      .           2         0         0
c01q03b003  qe         1       0         3         0     0      4      .           3         0         0
c01q04b004  qe         1       0         4         0     0      4      .           4         0         0
```

<!--
The meaning of the columns is as follows:

- `name` - name of the image file [str]
- `type` - stimulus type (`t` - test; `q` - localized question; `qe` - end-of-chapter question) [str]
- `ch_num` - chapter number [int]
- `pg_num` - page number within the chapter [int]
- `q_num_ch` - question number within the chapter [int]
- `q_num_pg` - question number within the page [int]
- `q_id` - question ID (e.g., from [Medusa](https://gitlab.com/ubiq-x/medusa) database, but can be any ID) [int]
- `a_cnt` - the number of answers choices to the question [int]
- `a_ord` - the order of answer choices (useful for randomized orderings) [str]
- `a_corr_num`- the ordinal number of the correct answer in the above ordering [int]
- `q_word_a` - the ordinal number of the first word from the answer passage [int]
- `q_word_b` - the ordinal number of the last word from the answer passage [int]
-->

The meaning of the columns is given in Table 1.

<p align="center"><b>Table 1.</b> Descriptions of columns in the stimulus presentation schedule text file.</p>

| Column | Description | Type |
| :--- | :--- | :--- |
| `name` | name of the image file | str |
| `type` | stimulus type (`t` - test; `q` - localized question; `qe` - end-of-chapter question) | str |
| `ch_num` | chapter number | int |
| `pg_num` | page number within the chapter | int |
| `q_num_ch` | question number within the chapter | int |
| `q_num_pg` | question number within the page | int |
| `q_id` | question ID (e.g., from [Medusa](https://gitlab.com/ubiq-x/medusa) database, but can be any ID) | int |
| `a_cnt` | the number of answers choices to the question | int |
| `a_ord` | the order of answer choices (useful for randomized orderings) | str |
| `a_corr_num` | the ordinal number of the correct answer in the above ordering | int |
| `q_word_a` | the ordinal number of the first word from the answer passage | int |
| `q_word_b` | the ordinal number of the last word from the answer passage | int |

<!--
The experiment relies on only the columns marked with the `+`.  For example, if `a_cnt` is equal to `2`, when presenting the associated question the experiment will only respond to keys `1` and `2` (other than the additional `A`, `B`, and `C` as described earlier).  The rest of the columns are copied into the results files and therefore can contain arbitrary values as long as it does not disturb the research in question.
-->

#### Probes Phase
The file [`eb/myfiles/1680x1050-en-eb-tbl-q0.txt`](eb/myfiles/1680x1050-en-eb-tbl-q0.txt) contains the schedule for the second trial (i.e., Phase 2) of the experiment.  This is how the first chapter of _Sense and Sensibility_ is encoded for the `1680x1050` screen resolution (tabs replaced with spaces for clarity):
```
name        type  ch_num  pg_num  q_num_ch  a_corr
title       t          0       0         0       0
c01z        t          1       0         0       0
c01p01      t          1       1         0       0
c01p02      t          1       2         0       0
c01p03      t          1       3         0       0
c01p04      t          1       4         0       0
c01p05      t          1       5         0       0
c01q01b001  qe         1       0         1       3
c01q02b002  qe         1       0         2       2
c01q03b003  qe         1       0         3       3
c01q04b004  qe         1       0         4       4
```
The columns are described in Table 1.  Because this phase of the experiment does not rely on localized questions, the schedule is simpler.

### Experiment Builder Variables
The following Experiment Builder variables (which are located on the very top of the graph) control certain aspects of the experiment:

- `CONST_EXP_P1_DUR` - Maximum duration of Phase 1 [ms] (enter `0` to skip Phase 1)
- `CONST_TIME_P_01` - Minimum probe time [ms]
- `CONST_TIME_P_02` - Maximum probe time [ms]
- `VAR_DISP_EOC_QUESTIONS` - Should end-of-chapter questions be displayed? (`{0,1}`)

### Results
The present experiment stores all result for every subject in a set of tab-delimited text files which are described below.  Of course, apart from those files, eye movements are also recorded and stored on the eye-tracking host computer (although they can optionally be transferred to the stimulus computer at the end of the experiment).

#### Localized Questions (Phase 1)
Select rows from the file [`analysis/data/reading/b.en/001/questions.txt`](analysis/data/reading/b.en/001/questions.txt) (tabs replaced with spaces and comments added for clarity):
```
stim       ans.correct  ans  is.correct         rt
c01p01q01            3    3           1  10277.716  # a correct answer provided in 10277.716 ms
c01p01q02            2    2           1   3563.880
...
c02p04q01            1    A           0   6374.547  # answer A
c02p05q01            2    2           1   5858.452
c03p01q01            4    B           0  15397.694  # answer B
...
```

#### End-of-Chapter Questions (Both Phases)
Select rows from the file [`analysis/data/reading/b.en/001/questions_eoc.txt`](analysis/data/reading/b.en/001/questions_eoc.txt) (tabs replaced with spaces and comments added for clarity):
```
phase  stim        ans.correct  ans  is.correct        rt
    1  c01q01b001            3    3           1  5756.016  # a correct answer provided in 5756.016 ms
    1  c01q02b002            2    2           1  6990.857
...
    1  c08q03b029            3    3           1  3406.940
    2  c09q01b030            4    4           1  9827.033  # beginning of Phase 2
    2  c09q02b031            3    3           1  2672.904
...
```

#### Stimuli (Both Phases)
Select rows from the file [`analysis/data/reading/b.en/001/stim.txt`](analysis/data/reading/b.en/001/stim.txt) (tabs replaced with spaces and comments added for clarity):
```
phase  stim        type           t0      t_disp
    1  title       t      177208.322   41439.421  # the book title page displayed 177208 sec after the experiment launch
    1  c01         t      233355.326    3628.585
    1  c01p01      t      237352.300  201588.541  # Page 1 of Chapter 1 displayed for 201588 sec
    1  c01p01q01   q      439308.400   10290.038
    1  c01p01q02   q      449966.400    3576.282  # a localized question answered in 3576.282 ms
...
    1  c08q03b029  qe    4707049.402    3418.923  # an end-of-chapter question answered in 3418.923 ms
    2  c09z        t      136924.455   11064.063  # beginning of Phase 2
    2  c09p01      t      148357.367  135580.891
...
```

#### Events (Phase 2)
This file lists all normal and mindless reading events (i.e., probes and self-reports) that occur in Phase 2.  Select rows from the file [`analysis/data/reading/b.en/001/evt.txt`](analysis/data/reading/b.en/001/evt.txt) (tabs replaced with spaces and comments added for clarity):
```
phase  stim    evt      t.phase     t.stim        rt
    2  c09p01  PN    202001.887  53653.619  3119.765  # probe-caught normal   reading discovered (probe responded to 3119.765 ms after it was displayed)
    2  c09p03  PZ    438229.120  61045.831  2631.864  # probe-caught mindless reading discovered (Z stands for "zoning out")
...
    2  c15p06  PZ   4260030.441  54581.161  1710.360  # probe-caught mindless reading discovered 4260030 sec after Phase 2 start
    2  c15p07  SZ   4372978.584  70958.321         .  # self-caught  mindless reading reported 70958 sec after the current stimulus (i.e., c15p07) presentation
```

#### Probe Timer (Phase 2)
This file lists times after which probes are scheduled to be displayed.  Note that a probe gets canceled and its timer reset after a self-report.  Select rows from the file [`analysis/data/reading/b.en/001/probe_timer.txt`](analysis/data/reading/b.en/001/probe_timer.txt) (tabs replaced with spaces and comments added for clarity):
```
       t
 89663.0  # the first  probe scheduled to be administered in  89663 ms
232894.0  # the second probe scheduled to be administered in 232894 ms
...
```
This file serves only the purpose of ensuring that the probe schedule was adhered to (i.e., as a double-check).


### Medusa Integration
In the context of the original research, the [Medusa](https://gitlab.com/ubiq-x/medusa) software was used to paginate the text and generate all stimuli images and stimuli presentation schedules.  If you are planning on making use of the present experiment, it would behoove you to familiarize yourself with Medusa as well as it may lead to substantial time savings.


## Data Analysis
In this section, I describe one way in which the results collected with the present experiment can be analyzed.  This analysis affords only cursory insight, but I think it is useful nevertheless, especially as an example of a starting point.

### Data Preparation
Because Experiment Builder stores each subject's results in separate directories, the first step is to copy all the LexTALE result file to one directory but preserving the subject IDs as the names of subdirectories because they become useful later.  The [`analysis/data/reading/`](analysis/data/reading/) directory contains an example of how that data might end up being structured (this is part of real data collected with the present experiment).

### Graphical Output
Once the data directory has been prepared, the [`analysis/analysis.R`](analysis/analysis.R) R script can be used to produce plots like this ones shown in Figures 3-9.

#### Material Exposition
<p align="center"><img src="analysis/out/img/mat-exp.reading-speed.png" width="50%" /></p>
<p align="center"><b>Figure 3.</b> Rate of page presentation provides an estimation of reading speed in the four language group.</p>

<p align="center"><img src="analysis/out/img/mat-exp.page-disp-t.png" width="50%" /></p>
<p align="center"><b>Figure 4.</b> Page display time provides an estimation of reading speed in the four language group.</p>

#### Mindless Reading Events
<p align="center"><img src="analysis/out/img/evt.evt-per-min.png" width="50%" /></p>
<p align="center"><b>Figure 5.</b> Distribution of the three events in the four language groups.</p>

<p align="center"><img src="analysis/out/img/evt.probes-per-min.png" width="50%" /></p>
<p align="center"><b>Figure 6.</b> Rate of probe presentation in the four groups.</p>

<p align="center"><img src="analysis/out/img/evt.probe-hit-ratio.png" width="50%" /></p>
<p align="center"><b>Figure 7.</b> Probe hit ratio (i.e., the proportion of the probes that discover subjects off-task) in the four groups.</p>

#### Questions
<p align="center"><img src="analysis/out/img/comp.inline.png" width="50%" /></p>
<p align="center"><b>Figure 8.</b> Text comprehension measured by the answers to localized (or in-line) questions in the four groups.</p>

<p align="center"><img src="analysis/out/img/comp.eoc.png" width="50%" /></p>
<p align="center"><b>Figure 9.</b> Text comprehension measured by the answers to end-of-chapter questions in the four groups.</p>


### Textual Output
The R script also generates plentiful textual output.  Some of that output corresponds to Figures 3-9.  For brevity, that textual output is not described here, but a sample output for the sample data can be found in the [`analysis/out/txt/`](analysis/out/txt/) directory.


## Dependencies
- The present experiment needs to be run by the [Experiment Builder](https://www.sr-research.com/experiment-builder)


## Setup
No setup is required; cloning this repository is enough.


## Acknowledgments
I worked as a post-doctoral research associate at the University of Pittsburgh when I developed this experiment.


## References

Baars, B. (1988).  _A cognitive theory of consciousness_.  Cambridge, England: Cambridge University Press.

Dehaene, S. & Naccache, L. (2001).  Towards a cognitive neuroscience of consciousness: Basic evidence and a workspace framework.  _Cognition, 79_, 1-37.

Grodsky, A., Giambra, L. (1989).  Task unrelated images and thoughts whilst reading.  In _Imagery: Current perspectives_.  New York: Plenum Press.

Killingsworth, M.A. & Gilbert, D.T. (2010).  A wandering mind is an unhappy mind.  _Science, 330(6006)_.

Loboda, T.D. (2014).  Study and detection of mindless reading.  _PhD Dissertation_.

Marian, V. & Spivey, M. (2003).  Bilingual and monolingual processing of competing lexical items.  _Applied Psycholinguistics, 24(2)_, 173–193.

Reichle, E.D., Reineberg, A.E., & Schooler, J.W. (2010).  Eye movements during mindless reading.  _Psychological Science, 21(9)_, 1300-10.

Sayette, M.A., Schooler, J.W., Reichle, E.D. (2009).  Out for a smoke: The impact of cigarette craving on zoning-out during reading.  _Psychological Science, 21(1)_, 26-30.

Schooler, J.W., Reichle, E.D., Halpern, D.V. (2004).  Zoning-out during reading: Evidence for dissociations between experience and meta-consciousness.  _Thinking and seeing: Visual metacognition in adults and children_ (pp. 203-226).  Cambridge, MA: MIT Press.

Smallwood, J., Fishman, D.J., & Schooler, J.W. (2007).  Counting the cost of an absent mind: Mind wandering as an under-recognized influence on educational performance.  _Psychonomic Bulletin & Review, 14_, 230-236.

Smallwood, J. & Schooler, J.W. (2006).  The restless mind.  _Psychological Bulletin, 132_, 946–958.

Thierry, G. & Wu, Y.J. (2007).  Brain potentials reveal unconscious translation during foreign-language comprehension.  _Proceedings of the National Academy of Sciences of the United States of America, 104(30)_, 12530–12535.


## Citing
Loboda, T.D. (2016).  Eye Movements of Bilinguals during Mindful and Mindless Reading [Experiment Builder experiment].  Available at https://gitlab.com/ubiq-x/mindless-read-biling


## License
This project is licensed under the [BSD License](LICENSE.md).
