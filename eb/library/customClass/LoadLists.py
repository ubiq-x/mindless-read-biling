##############################################################################
# CustomClass Rules                                                          #
# =================                                                          #
#                                                                            #
#1. All custom classes must inherit sreb.EBObject and the constructor        #
#   must call sreb.EBObject's constructor. If a class starts with _ then it  #
#   is considered internal and will not be treated as a custom class.        #
#                                                                            #
#2. The custom class will only use the default constructor.                  #
#   ie. a constructor with no parameters.                                    #
#                                                                            #
#3. To add a property provide getter and have an attribute that does not     #
#   starts with an underscore(_) or an upper case letter, and have the       #
#   attribute of a know type. Known types are int,float,str,EBPoint,EBColor, #
#   tuple, and list.                                                         #
#   If an attribute's default value is of type tuple and only has two        #
#   items, the property will be treated as an EBPoint. Similarly, if an      #
#   attribute's default value is a tuple of 3 items the property will be     #
#   treated as an EBColor.  The input type of the setter and the output type #
#   of the getter is expected to be the same as the type of the attribute.   #
#                                                                            #
#4. If only getter is provided, the property will be a readonly property.    #
#                                                                            #
#6. The custom class may be instanciated during development to obtain        #
#   class info. Avoid such things as display mode change, remote connections #
#   in the constructor.                                                      #
#                                                                            #
#7. Any method in the custom class can be called using the Execute action    #
#   By default, the return type of the method is string unless a doc string  #
#   with the following constraint is available                               #
#	a. The doc string starts with "RETURN:" (case matters)               #
#       b. Following the text "RETURN:" provide a default value of the type  #
#          or the __repr__ value of the class. eg. str for string            #
#8. If a property's setter metthod has default values for it's parameters,   #
#    the property will not accept references or equation.                    #
##############################################################################


import sreb
import os

class CustomClassTemplate(sreb.EBObject):
	def __init__(self):
		sreb.EBObject.__init__(self)

		self.idxP1 = 0
		self.idxP2 = 0
		self.lstP1 = [["title","t",0,0,0,0,0,0,".",0,0,0],["c01","t",1,0,0,0,0,0,".",0,0,0]]
		self.lstP2 = [["title","t",0,0,0,0],["c01z","t",1,0,0,0]]

	def loadLst(self, scrW, scrH, lang, phase):
		if phase == 1:
			self.lstP1 = list()
		else:
			self.lstP2 = list()

		fpath = "./myfiles/" + str(scrW) + "x" + str(scrH) + "-" + str(lang) + (phase == 1 and "-eb-tbl-q1-random.txt" or "-eb-tbl-q0.txt")

		if os.path.exists(fpath) == False:
			print "File doesn't exist: ", fpath
			return 0;

		f=open(fpath, "r")
		for l in f.readlines():
			if len(l) < 1:
				break
			else:
				(phase == 1 and self.lstP1.append(l.split()) or self.lstP2.append(l.split()))
		f.close()

	def nextP1(self):
		res = self.lstP1[self.idxP1]

		if (self.idxP1 < len(self.lstP1) - 1): self.idxP1 += 1
		else: self.idxP1 = len(self.lstP1) - 1

		return res

	def nextP2(self):
		res = self.lstP2[self.idxP2]

		if (self.idxP2 < len(self.lstP2) - 1): self.idxP2 += 1
		else: self.idxP2 = len(self.lstP2) - 1

		return res
